const antlr4 = require("antlr4");
const RegexLexer = require("./gen/RegexLexer").RegexLexer;
const RegexParser = require("./gen/RegexParser").RegexParser;
const RegexListener = require("./gen/RegexListener").RegexListener;

// This enum gives us a unique object we can detect. You can extend it
// with more cases if your implementation needs it.
const Exprs = Object.freeze({
  ALT: "ALT",
  LITERAL_EXPR: "LITERAL_EXPR",
  GROUP_EXPR: "GROUP_EXPR",
  SIMPLE_GROUP: "SIMPLE_GROUP",
  NESTED_GROUP: "NESTED_GROUP",
  ALT_GROUP: "ALT_GROUP",
  LITERAL_GROUP: "LITERAL_GROUP"
});

// These names are just unique identifiers we'll use when creating the highlighting
// DOM structure. You shouldn't need to modify them.
const sourceName = "hoverSource";
const targetName = "hoverTarget";

// You shouldn't need to change any of the labeling or hoverScript code to get the
// compiler working. The rest of the code should make it clear how to use it.
function labeledSpan(text, label) {
  return '<span class="' + label + '">' + text + "</span>";
}

// To use the ANTLR4 JavaScript backend, we extend the listener class that has
// been generated for our grammar. By overriding certain methods of this class, we
// control what happens when the compiler walks over the parse tree.
class CompilerListener extends RegexListener {
  constructor() {
    super();
    // This compiler uses a stack to keep track of partial results from
    // walking over a node's children.
    this.stack = [];
    this.levels = [];
    this.current = undefined;
    this.currentLabel = 1;
    this.script = "";
    this.result = "";
  }

  nextLabelPair() {
    const labelID = this.currentLabel++;
    return [sourceName + labelID, targetName + labelID];
  }

  // You shouldn't need to change this
  hoverScript(source, target, color) {
    this.script += (
      `$('.${source}').hover(function(){$('.${target}').css('background','${color}');},
                                   function(){$('.${target}').css('background','');});` +
      `$('.${source}').hover(function(){$('.${source}').css('background','cyan');},
                                         function(){$('.${source}').css('background','');});`
    ).replace(/\s/g, "");
  }

  // Compiling an atom is easy: we just want to print it out as text. We can access
  // the token as ctx.c because we named it that way in the grammar. Lexer rule contexts
  // hold their text in the "text" field; for parser rules you can get the text of the
  // expression by calling the .getText() method.

  // We push the text onto the compiler stack so that parent rules can do things with it.
  exitAtom(ctx) {
    this.stack.push(ctx.c.text);
  }

  // The element rule in the grammar has two cases. For the first case, we don't need to
  // do anything because of our handler for atoms (we would just push the same text back
  // on the stack). Since we've names the cases, we can write a handler specifically for
  // the second one.

  // If an atom has a modifier, we need to do something to highlight it. To do that we
  // get the text of the atom from the top of the stack, wrap it in appropriate labeled
  // spans, and push it back. We also need to register some code to do the highlighting.
  exitModifiedElement(ctx) {
    // this just makes two unique labels (as strings)
    const [source, target] = this.nextLabelPair();
    // get the text of the atom we want to decorate with a modifier
    const atom = this.stack.pop();
    // get the modifier token
    const modifier = ctx.m.text;

    // We label the atom as the target, and the modifier as the source. This means
    // that when we mouse over the source in the browser, we'll see highlighting
    // on the target (and cyan highlighting on the source).
    const htmlBody = labeledSpan(atom, target) + labeledSpan(modifier, source);

    // This just generates some JQuery to register the syntax highlighting effect.
    // The color is for the highlighting of the target; the source is always cyan.
    this.hoverScript(source, target, "blue");

    this.stack.push(htmlBody);
  }

  getLeaves(type) {
    let leaves = [];
    for (
      let expr = this.stack.pop();
      expr != undefined && expr !== type;
      expr = this.stack.pop()
    ) {
      leaves.unshift(expr);
    }

    return leaves;
  }

  enterAlt(ctx) {
    if (this.current === Exprs.ALT) {
      this.levels[0] += 1;
      return;
    }
    this.levels.unshift(0);
    this.current = Exprs.ALT;
    this.stack.push(Exprs.ALT);
  }

  exitAlt(ctx) {
    if (this.levels[0] > 0) {
      this.levels[0] -= 1;
      return;
    }
    this.levels.shift();
    const [source, target] = this.nextLabelPair();
    this.hoverScript(source, target, "yellow");

    const leaves = this.getLeaves(Exprs.ALT);
    const htmlBody = leaves
      .map(x => labeledSpan(x, target))
      .join(labeledSpan("|", source));

    this.stack.push(htmlBody);
  }

  enterLiteralExpr(ctx) {
    this.stack.push(Exprs.LITERAL_EXPR);
  }

  exitLiteralExpr(ctx) {
    const leaves = this.getLeaves(Exprs.LITERAL_EXPR);
    this.stack.push(leaves.join(""));
  }

  enterGroupExpr(ctx) {
    this.current = Exprs.GROUP_EXPR;
    this.stack.push(Exprs.GROUP_EXPR);
  }

  exitGroupExpr(ctx) {
    const [source, target] = this.nextLabelPair();
    this.hoverScript(source, target, "orange");
    const leaves = this.getLeaves(Exprs.GROUP_EXPR);
    const left = leaves[0];
    const group = leaves[1];
    const right = leaves[2];
    let htmlBody =
      labeledSpan("(", source) +
      labeledSpan(group, target) +
      labeledSpan(")", source);
    const modifier = ctx.m && ctx.m.text;
    if (modifier) {
      const [source, target] = this.nextLabelPair();
      this.hoverScript(source, target, "blue");

      htmlBody = labeledSpan(htmlBody, target) + labeledSpan(modifier, source);
    }

    htmlBody = left + htmlBody + right;

    this.stack.push(htmlBody);
  }

  enterSimpleGroup(ctx) {
    this.current = Exprs.SIMPLE_GROUP;
    this.stack.push(Exprs.SIMPLE_GROUP);
  }

  exitSimpleGroup(ctx) {
    const [source, target] = this.nextLabelPair();
    this.hoverScript(source, target, "orange");

    const groupExpr = this.getLeaves(Exprs.SIMPLE_GROUP).join("");

    let htmlBody =
      labeledSpan("(", source) +
      labeledSpan(groupExpr, target) +
      labeledSpan(")", source);

    const modifier = ctx.m && ctx.m.text;
    if (modifier) {
      const [source, target] = this.nextLabelPair();
      this.hoverScript(source, target, "blue");

      htmlBody = labeledSpan(htmlBody, target) + labeledSpan(modifier, source);
    }

    this.stack.push(htmlBody);
  }

  enterNestedGroup(ctx) {
    this.current = Exprs.NESTED_GROUP;
    this.stack.push(Exprs.NESTED_GROUP);
  }

  exitNestedGroup(ctx) {
    const leaves = this.getLeaves(Exprs.NESTED_GROUP);
    this.stack.push(leaves.join(""));
  }

  enterAltGroup(ctx) {
    if (this.current === Exprs.ALT_GROUP) {
      this.levels[0] += 1;
      return;
    }
    this.levels.unshift(0);
    this.current = Exprs.ALT_GROUP;
    this.stack.push(Exprs.ALT_GROUP);
  }

  exitAltGroup(ctx) {
    if (this.levels[0] > 0) {
      this.levels[0] -= 1;
      return;
    }
    this.levels.shift();

    const [source, target] = this.nextLabelPair();
    this.hoverScript(source, target, "yellow");

    const leaves = this.getLeaves(Exprs.ALT_GROUP);
    const htmlBody = leaves
      .map(x => labeledSpan(x, target))
      .join(labeledSpan("|", source));

    this.stack.push(htmlBody);
  }

  enterLiteralGroup(ctx) {
    this.current = Exprs.LITERAL_GROUP;
    this.stack.push(Exprs.LITERAL_GROUP);
  }

  exitLiteralGroup(ctx) {
    const leaves = this.getLeaves(Exprs.LITERAL_GROUP);
    this.stack.push(leaves.join(""));
  }

  // This handler fires when we finish the walk and exit the root "re" rule. All it
  // needs to do is provide the result from the single child.
  exitRe(ctx) {
    this.result = this.stack.pop();
  }
}

// This is the magic incantation to set up the compiler pipeline
// and run both the parser and the tree walk. We export the just the
// compile function (which takes a regexp string as input and returns
// the labeled html spans and the registration script which needs to be run).
// index.html knows what to do with this once you do "npm build" to run browserify.

function lexAndParse(input) {
  const chars = new antlr4.InputStream(input);
  const lexer = new RegexLexer(chars);
  const tokens = new antlr4.CommonTokenStream(lexer);
  const parser = new RegexParser(tokens);
  parser.buildParseTrees = true;
  const tree = parser.re();
  return tree;
}

function compile(input) {
  const tree = lexAndParse(input);
  const compiler = new CompilerListener();
  antlr4.tree.ParseTreeWalker.DEFAULT.walk(compiler, tree);
  return [compiler.result, compiler.script];
}

module.exports = compile;
