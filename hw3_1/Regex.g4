grammar Regex;

re
    : alt EOF
    | expression EOF
    ;

alt
    : alt ALT expression        
    | expression ALT expression
    ;

expression
    : (elems+=element)* # LiteralExpr
    | expression LPAREN group RPAREN (m=MODIFIER) expression # GroupExpr
    ;
    
group
    : LPAREN group RPAREN (m=MODIFIER)? # SimpleGroup
    | group group # NestedGroup
    | group ALT group # AltGroup
    | (elems+=element)+ # LiteralGroup
    ;

element
    : a=atom            # AtomicElement
    | a=atom m=MODIFIER # ModifiedElement
    ;

atom
    : c=CHARACTER
    ;

CHARACTER : [a-zA-Z0-9.] ;
MODIFIER : [?*] ;
ALT: [|] ;
LPAREN: [(] ;
RPAREN: [)] ;
WS : [ \t\r\n]+ -> skip ;
